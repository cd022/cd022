#include <stdio.h>
int main()
{
  int i,n,sum=0;
  printf("Input number of terms: ");
  scanf("%d",&n);
  printf("\nThe first %d even numbers are:",n);
  for(i=1;i<=n;i++)
  {
    printf("%d ",2*i);
    sum+=(2*i)*(2*i);
  }
  printf("\nThe sum of square of first %d even number is:%d \n",n,sum);
  return 0;
} 
