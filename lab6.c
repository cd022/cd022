#include <stdio.h>
int main()
{
	int marks[5][3], i, j, maxmarks; 
	for(i=0; i<5; i++) 
	{
		printf("Enter the marks obtained by student %d",i+1); 
		for(j=0; j<3; j++)
		{
			printf("\n Marks[%d][%d] = ", i, j);
			scanf("%d", &marks[i][j]);
		}
	} 
	for(j=0; j<3; j++) 
	{
		maxmarks=marks[0][j];
		for(i=0; i<5; i++)
		{
			if(marks[i][j]>maxmarks)
				maxmarks = marks[i][j];
		}
		printf("\n The highest marks obtained in the subject %d = %d", j+1, maxmarks); 
	}
	return 0;
}
