#include <stdio.h>
int main()
{
  int n; 
  float sum=0, i, j;
  printf("Enter the value of number: ");
  scanf ("%d", &n);
  for(j=1;j<=n;j++)
  {
    i=1/j;
    sum = sum +i*i;
  }
  printf("The sum of series 1/1^2 + 1/2^2 +.... + l/%d^2 = %.2f",n,sum);
  return 0;
}
